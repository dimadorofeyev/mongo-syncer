# Mongo sync tool 
[![pipeline status](https://gitlab.com/dimadorofeyev/mongo-syncer/badges/master/pipeline.svg)](https://gitlab.com/dimadorofeyev/mongo-syncer/-/commits/master)

## About

`mongo-syncer` was created to migrate mongo databases from on-premis to k8s and be able to sync them in live.
This tool is able to work as a daemon and does live sync in a background.
You can choose databases what need to sync. By default it syncs all databases.
It retrives secrets from on-premis and from Azure key vault and do all magic automatically.
More info could be found [here](src/README.md)

## Requirements

- ansible ~>2.7
- azure key vault with secrets to k8s mongodb
- mongodb installed in on-prem with running in docker
- mongodb installed in k8s with the same name as on-prem

## Usage

1. Create value files with right endpoints and credentials helm/values/<env>/values.yaml

    ```yaml
    # source database config
    src:
      hosts: "sorce.host:27017" # hostportstr of a member of replica set
      authdb: "admin"
      username: "admin"
      # password: "db-password" # could be empty if no

    # destination database config
    dst:
      hosts: "dest.host:27017" # hostportstr of standalone, mongos or a member of replica set
      authdb: "admin"
      username: "admin"
      # password: "db-password" # could be empty if no

    # Sync options
    # Database to sync
    # If commented this block will sync all dbs
    dbs:
      - yourdb1
      - yourdb2
    ```

2. Install helm chart

    ```txt
    > helm3 upgrade --install <release-name> -n <namespace> -f ../values/<env>/values.yaml .
    ```

3. Check pod logs

## Automation with Ansible

There were created ansible playbook to automate creation helm value file for mongo-syncer.

## Usage

1. Set VM host from which migrate MongoDB to `ansible/hosts.ini`
2. Set `dst_host` VM host in `ansible/group_vars/all.yaml` if you want to migrate to VM
3. If you want to generate for k8s you can do the trick and use service as `dst_host`. Just replace `dst_host` to `db_name` in template `ansible/roles/generator/templates/values.j2`
4. Run playbook

   ```txt
   ansible-playbook generate.yaml -i hosts.ini -l <env> \
        -e "ansible_user=<pnl0xxxx> ansible_password=<your-password>" \
        -e "az_client_id=<az_client_id> az_tenant=<az_tenant> az_secret=<az_secret> az_subscription_id=<az_subscription_id>"
   ```

    After ansible-playbook running you will get list of helm values files that could be applied synconization process.
    For bulk migration databases you can run:

    ```txt
    cd ansible/values
    for file in $(ls | grep yaml | cut -d '.' -f 1); do
      helm3 upgrade -i ${file} --namespace mongosync -f ${file}.yaml ../../helm/mongo-syncer --create-namespace
    done
    ```
